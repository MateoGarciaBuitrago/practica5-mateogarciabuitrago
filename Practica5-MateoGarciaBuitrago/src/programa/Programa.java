package programa;

import java.util.Scanner;

import clases.AdminConcierto;


public class Programa {
/**
 * Inicio del programa
 * @param args
 */
	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		int opcion;
		/**
		 * Creo un administrador fuera del menu para poder utilizarlo en cada uno de los cases
		 * Ademas, doy de alta un artista y un concierto para poder usar el metodo 9, que necesita tener un concierto con su artista fijado.
		 */
		AdminConcierto administrador = new AdminConcierto();
		administrador.altaArtista("Travis Scott", 28, "Trap, rap, ", "Estadounidense", 'S', "Travis", "11D");
		administrador.altaConcierto("Coachella",185, 180000, 1, 20, "Arkansas", 120000, "2021-08-27");
		administrador.fijarConciertoArtista("11D", "Coachella");
		do {
			/**
			 * Se inicia el menu con cada una de las opciones
			 */
			System.out.println("__________________________________________________");
			System.out.println("               Menu              ");
			System.out.println("1.- Crear instancia de GestorClases. ");
			System.out.println("2.- Dar de alta 3 elementos de Clase1 y listar.");
			System.out.println("3.- Buscar un elemento de Clase1.");
			System.out.println("4.- Eliminar un elemento de Clase1 y listar.");
			System.out.println("5.- Dar de alta 3 elementos de Clase2 y listar.");
			System.out.println("6.- Buscar un elemento de Clase2.");
			System.out.println("7.- Eliminar un elemento de Clase2 y listar.");
			System.out.println("8.- Lista elementos Clase2 por alg�n atributo.");
			System.out.println("9.- Listar elementos Clase2 con un elemento de Clase1.");
			System.out.println("10.- Asignar un elemento que use Clase1 y Clase2 y listar.");
			System.out.println("11.- M�todo extra 1");
			System.out.println("12.- M�todo extra 2");
			System.out.println("13.- M�todo extra 3");
			System.out.println("14.- M�todo extra 4");
			System.out.println("15.- Salir");
			System.out.println("___________________________________________________");

			opcion = lector.nextInt();
			lector.nextLine();

			switch (opcion) {
			/**
			 * Aqui comienza el switch, donde segun la opcion que introduzcamos por teclado, iniciara una u otra
			 * Se recomienda en primer lugar dar de alta los elementos de las clases, en este caso Artistas "2" y Conciertos "5"
			 * Asi podremos usar los metodos correspondientes
			 */
			case 1:
				AdminConcierto administradorPrueba = new AdminConcierto();
				System.out.println(administradorPrueba.gestorClases());
				break;
			case 2:
				administrador.altaArtista("Anton Alvarez", 31, "Trap, rap, variado", "Espanol", 'A', "C.Tangana", "11A");
				administrador.altaArtista("Benito Martinez", 26, "Trap, reggaeton", "Puertorriqueno", 'S', "Bad Bunny", "11B");
				administrador.altaArtista("Dua Lipa", 25, "Pop", "Britanica", 'S', "Dula peep", "11C");
				administrador.listarArtistas();
				break;
				
			case 3:
				System.out.println("Buscar artista");
				System.out.println("Introduce el DNI del artista");
				String artista = lector.nextLine();
				System.out.println(administrador.buscarArtista(artista));
				
				break;
			case 4:
				System.out.println("Eliminar artista");
				System.out.println("Introduce el DNI del artista");
				String artistaEliminado = lector.nextLine();
				administrador.eliminarArtista(artistaEliminado);
				administrador.listarArtistas();
				break;
			case 5:
				administrador.altaConcierto("VinaRock", 25, 10000, 2, 20, "Albacete", 60000, "2021-04-17");
				administrador.altaConcierto("Arenal Sound", 105.20, 25000, 1, 30, "Valencia", 70000, "2021-07-10");
				administrador.altaConcierto("Tomorrowland", 350, 150000, 3, 05, "Boom", 400000, "2021-08-22");
				administrador.listarConciertos();
				break;
			case 6:
				System.out.println("Buscar concierto");
				System.out.println("Introduce el nombre del concierto");
				String concierto = lector.nextLine();
				System.out.println(administrador.buscarConcierto(concierto));
				
				break;
			case 7:
				System.out.println("Eliminar concierto");
				System.out.println("Introduce el nombre del concierto");
				String conciertoEliminado = lector.nextLine();
				administrador.eliminarConcierto(conciertoEliminado);
				administrador.listarConciertos();
				break;
			case 8:
				System.out.println("Introduce el aforo");
				int aforo = lector.nextInt();
				lector.nextLine();
				administrador.listarConciertoAforo(aforo);
				break;
			case 9:
				System.out.println("Introduce el DNI del artista para listar sus conciertos asignados");
				String artistaConcierto = lector.nextLine();
				System.out.println(administrador.buscarArtista(artistaConcierto));
				administrador.listarConciertosArtista(artistaConcierto);
				break;
			case 10:
				System.out.println("Introduce el DNI del artista ");
				String asignarArtistaConcierto = lector.nextLine();
				System.out.println("Introduce el nombre del concierto que le quieres asignar");
				String asignarConciertoArtista = lector.nextLine();
				administrador.fijarConciertoArtista(asignarArtistaConcierto, asignarConciertoArtista);
				System.out.println("Mostramos el resultado introduciendo");
				administrador.listarConciertosArtista(asignarArtistaConcierto);
				break;
			case 11:
				System.out.println("Mostramos el coste promedio de las entradas");
				System.out.println(administrador.costePromedioEntrada());
				break;
			case 12:
				System.out.println("Comprobar cache");
				System.out.println("Introduce el dni del artista");
				String dni = lector.nextLine();
				administrador.ArtistasTop(dni);
				break;
			case 13:
				System.out.println("Vamos a dar a los artistas lo que se merecen");
				System.out.println("Introduce el nombre del concierto");
				String conciertoHonorario = lector.nextLine();
				System.out.println(administrador.honorarioArtistas(conciertoHonorario));
				break;
			case 14:
				System.out.println("Cambiar el nombre del artista");
				System.out.println("Introduce el DNI del artista ");
				String dniArtista = lector.nextLine();
				System.out.println("Introduce su nuevo nombre ");
				String nuevoNombre = lector.nextLine();
				administrador.cambiarNombreArtista(dniArtista, nuevoNombre);
				administrador.listarArtistas();
				break;
			case 15:
				System.exit(0);
				break;
			
			default:
				System.out.println("Opcion no contemplada");
				break;
			}
		} while (opcion != 15);
		
		lector.close();


	}
	

}
