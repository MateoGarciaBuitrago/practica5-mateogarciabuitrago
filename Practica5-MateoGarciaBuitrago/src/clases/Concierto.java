package clases;

import java.time.LocalDate;

/**
 * Comenzamos con la declaracion de las variables asignando el tipo que se adecue correctamente
 * @author mateo
 *
 */
public class Concierto {

	 private String nombre;
	 private LocalDate fechaConcierto;
	 private double precioEntrada;
	 private double honorarioArtista;
	 private int duracionHoras;
	 private int duracionMinutos;
	 private String ciudad;
	 private int aforoMax;
	 private Artista artistaConcierto;
	 
	 /**
	  * Inicio el constructor con los parametros minimos que yo deseo
	  * @param nombre
	  * @param precioEntrada
	  * @param honorarioArtista
	  * @param duracionHoras
	  * @param duracionMinutos
	  * @param ciudad
	  * @param aforoMax
	  */
	public Concierto(String nombre, double precioEntrada, double honorarioArtista,
			int duracionHoras, int duracionMinutos, String ciudad, int aforoMax) {
	
		super();
		this.nombre = nombre;
		this.precioEntrada = precioEntrada;
		this.honorarioArtista = honorarioArtista;
		this.duracionHoras = duracionHoras;
		this.duracionMinutos = duracionMinutos;
		this.ciudad = ciudad;
		this.aforoMax = aforoMax;
	}
/**
 * Genero los setters y getters, que me ayudaran a trabajar con los metodos en el gestor
 * @return
 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaConcierto() {
		return fechaConcierto;
	}

	public void setFechaConcierto(LocalDate fechaConcierto) {
		this.fechaConcierto = fechaConcierto;
	}

	public double getPrecioEntrada() {
		return precioEntrada;
	}

	public void setPrecioEntrada(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}

	public double getHonorarioArtista() {
		return honorarioArtista;
	}

	public void setHonorarioArtista(double honorarioArtista) {
		this.honorarioArtista = honorarioArtista;
	}

	public int getDuracionHoras() {
		return duracionHoras;
	}

	public void setDuracionHoras(int duracionHoras) {
		this.duracionHoras = duracionHoras;
	}

	public int getDuracionMinutos() {
		return duracionMinutos;
	}

	public void setDuracionMinutos(int duracionMinutos) {
		this.duracionMinutos = duracionMinutos;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public int getAforoMax() {
		return aforoMax;
	}

	public void setAforoMax(int aforoMax) {
		this.aforoMax = aforoMax;
	}

	public Artista getArtistaConcierto() {
		return artistaConcierto;
	}

	public void setArtistaConcierto(Artista artistaConcierto) {
		this.artistaConcierto = artistaConcierto;
	}
/**
 * Por ultimo genero el toString
 */
	@Override
	public String toString() {
		return "Concierto [nombre=" + nombre + ", fechaConcierto=" + fechaConcierto + ", precioEntrada=" + precioEntrada
				+ ", honorarioArtista=" + honorarioArtista + ", duracionHoras=" + duracionHoras + ", duracionMinutos="
				+ duracionMinutos + ", ciudad=" + ciudad + ", aforoMax=" + aforoMax + ", artistaConcierto="
				+ artistaConcierto + "]";
	}
	 
}