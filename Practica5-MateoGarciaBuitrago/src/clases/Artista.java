package clases;

/**
 * Del mismo modo que en concierto, declaro las variables que voy a utilizar para Artista
 * @author mateo
 *
 */
public class Artista {
	
	private String nombre;
	private String apellido1;
	private String apellido2;
	private int edad;
	private String grupo;
	private String generoMusical;
	private String nacionalidad;
	private char cache;
	private String nick;
	private String dni;
	
	/**
	 * Creo su constructor
	 * @param nombre
	 * @param edad
	 * @param generoMusical
	 * @param nacionalidad
	 * @param cache
	 * @param nick
	 * @param dni
	 */
	
	public Artista(String nombre,int edad, String generoMusical,
			String nacionalidad, char cache, String nick, String dni) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.generoMusical = generoMusical;
		this.nacionalidad = nacionalidad;
		this.cache = cache;
		this.nick = nick;
		this.dni = dni;
	}
	/**
	 * Creo de nuevo otro constructor con mas datos adicionales
	 * @param nombre
	 * @param apellido1
	 * @param apellido2
	 * @param edad
	 * @param grupo
	 * @param generoMusical
	 * @param nacionalidad
	 * @param cache
	 * @param nick
	 */
	public Artista(String nombre, String apellido1, String apellido2, int edad, String grupo, String generoMusical,
			String nacionalidad, char cache, String nick) {
		super();
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.edad = edad;
		this.grupo = grupo;
		this.generoMusical = generoMusical;
		this.nacionalidad = nacionalidad;
		this.cache = cache;
		this.nick = nick;
		
	}
/**
 * Genero los setters y getters
 * @return
 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getGeneroMusical() {
		return generoMusical;
	}

	public void setGeneroMusical(String generoMusical) {
		this.generoMusical = generoMusical;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public char getCache() {
		return cache;
	}

	public void setCache(char cache) {
		this.cache = cache;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
/**
 * Por ultimo genero el toString
 */
	@Override
	public String toString() {
		return "Artista [nombre=" + nombre + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", edad=" + edad
				+ ", grupo=" + grupo + ", generoMusical=" + generoMusical + ", nacionalidad=" + nacionalidad
				+ ", cache=" + cache + ", nick=" + nick + ", dni=" + dni + "]";
	}
	
	
}
