package clases;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Iterator;

/**
 * Esta clase va a ser el gestor, la cual va a trabajar con dos ArrayList las
 * otras clases creadas Declaro los dos ArrayList
 * 
 * @author mateo
 *
 */
public class AdminConcierto {

	private ArrayList<Concierto> listaConciertos;
	private ArrayList<Artista> listaArtistas;

	/**
	 * El constructor unicamente contiene la creacion de los arraysList
	 */
	public AdminConcierto() {
		super();
		this.listaConciertos = new ArrayList<Concierto>();
		this.listaArtistas = new ArrayList<Artista>();
	}

	/**
	 * Genero el metodo de creacion de alta de artistas Ya que estos metodos los
	 * hemos realizado en clase, unicamente iniciare el comentario
	 * 
	 * @param nombre
	 * @param edad
	 * @param generoMusical
	 * @param nacionalidad
	 * @param cache
	 * @param nick
	 * @param dni
	 */
	public void altaArtista(String nombre, int edad, String generoMusical, String nacionalidad, char cache, String nick,
			String dni) {

		if (!artistaCreado(dni)) {
			Artista nuevoArtista = new Artista(nombre, edad, generoMusical, nacionalidad, cache, nick, dni);
			listaArtistas.add(nuevoArtista);
		} else {
			System.out.println("El artista ya existe");
		}

	}

	/**
	 * Listar los artistas
	 */
	public void listarArtistas() {
		for (Artista artista : listaArtistas) {
			if (artista != null) {
				System.out.println(artista);
			}
		}
	}

	/**
	 * Comprobar si un artista esta creado, lo utiliza altaArtista
	 * 
	 * @param dni
	 * @return
	 */
	public boolean artistaCreado(String dni) {
		for (Artista artista : listaArtistas) {
			if (artista != null && artista.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Eliminar un artista por DNI
	 * 
	 * @param dni
	 */
	public void eliminarArtista(String dni) {
		Iterator<Artista> iteradorArtistas = listaArtistas.iterator();

		while (iteradorArtistas.hasNext()) {
			Artista artista = iteradorArtistas.next();
			if (artista.getDni().equals(dni)) {
				iteradorArtistas.remove();
			}
		}

	}

	/**
	 * Buscar un artista por DNI
	 * 
	 * @param dni
	 * @return
	 */
	public Artista buscarArtista(String dni) {
		for (Artista artista : listaArtistas) {
			if (artista != null && artista.getDni().equals(dni)) {
				return artista;
			}
		}
		return null;
	}

	/**
	 * Dar de alta un Concierto
	 * 
	 * @param nombre
	 * @param precioEntrada
	 * @param honorarioArtista
	 * @param duracionHoras
	 * @param duracionMinutos
	 * @param ciudad
	 * @param aforoMax
	 * @param fechaConcierto
	 */
	public void altaConcierto(String nombre, double precioEntrada, double honorarioArtista, int duracionHoras,
			int duracionMinutos, String ciudad, int aforoMax, String fechaConcierto) {

		if (!conciertoCreado(nombre)) {
			Concierto nuevoConcierto = new Concierto(nombre, precioEntrada, honorarioArtista, duracionHoras,
					duracionMinutos, ciudad, aforoMax);
			nuevoConcierto.setFechaConcierto(LocalDate.parse(fechaConcierto));
			listaConciertos.add(nuevoConcierto);
		} else {
			System.out.println("El concierto ya existe");
		}

	}

	/**
	 * Listar conciertos
	 */
	public void listarConciertos() {
		for (Concierto concierto : listaConciertos) {
			if (concierto != null) {
				System.out.println(concierto);
			}
		}
	}

	/**
	 * Comprobar si concierto existe, lo utiliza altaConcierto
	 * 
	 * @param nombre
	 * @return
	 */
	public boolean conciertoCreado(String nombre) {
		for (Concierto concierto : listaConciertos) {
			if (concierto != null && concierto.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Eliminar un concierto por Nombre
	 * 
	 * @param nombre
	 */
	public void eliminarConcierto(String nombre) {
		Iterator<Concierto> iteradorConcierto = listaConciertos.iterator();

		while (iteradorConcierto.hasNext()) {
			Concierto concierto = iteradorConcierto.next();
			if (concierto.getNombre().trim().toLowerCase().equalsIgnoreCase(nombre)) {
				iteradorConcierto.remove();
			}
		}

	}

	/**
	 * Buscar un concierto por nombre
	 * 
	 * @param nombre
	 * @return
	 */
	public Concierto buscarConcierto(String nombre) {
		for (Concierto concierto : listaConciertos) {
			if (concierto != null && concierto.getNombre().trim().toLowerCase().equalsIgnoreCase(nombre)) {
				return concierto;
			}
		}
		return null;
	}

	/**
	 * Listar los concerto por su aforo Solamente lo listara si el aforo introducido
	 * por teclado es mayor o igual a los conciertos ya creados
	 * 
	 * @param aforoMax
	 */
	public void listarConciertoAforo(int aforoMax) {
		for (Concierto concierto : listaConciertos) {
			if (concierto.getAforoMax() >= aforoMax) {
				System.out.println(concierto);
			}
		}

	}

	/**
	 * Listamos los conciertos que tiene asignado un artista Necesitaremos primero
	 * que un artista tenga un conciert fijado
	 * 
	 * @param dni
	 */
	public void listarConciertosArtista(String dni) {
		for (Concierto concierto : listaConciertos) {
			if (concierto.getArtistaConcierto() != null && concierto.getArtistaConcierto().getDni().equals(dni)) {
				System.out.println(concierto);
			}
		}

	}

	/**
	 * Asignamos un concierto a un artista Lo utiliza el metodo
	 * listarConciertosArtisa
	 * 
	 * @param dni
	 * @param nombre
	 */
	public void fijarConciertoArtista(String dni, String nombre) {
		if (buscarArtista(dni) != null && buscarConcierto(nombre) != null) {
			Artista artista = buscarArtista(dni);
			Concierto concierto = buscarConcierto(nombre);
			concierto.setArtistaConcierto(artista);
		}

	}

	/**
	 * Este metodo unicamente lo utilizo para iniciar la instancia y que no salte un
	 * warning por no usar el adminConciertoPrueba
	 * 
	 * @return
	 */
	public String gestorClases() {
		return "Gestor de clases";
	}

	/**
	 * Este metodo recorre la lista de conciertos y calcula la media segun el precio
	 * de la entrada y el numero de conciertos que haya
	 * 
	 * @return Devuelve la media
	 */
	public double costePromedioEntrada() {
		int media = 0;
		for (Concierto concierto : listaConciertos) {
			media += concierto.getPrecioEntrada();
		}
		return (media / listaConciertos.size());
	}

	/**
	 * Este metodo es el mas elaborado, por ello tambien puede que de error Los
	 * artistas reciben una clasificacion de cache, "S, A, B, C, D, etc", ordenados
	 * de mayor a mayor Por lo que he considerado que los conciertos de estos
	 * artistas deben de tener un precio superior prefijado Si tienen S, la entrada
	 * valdra 250, si es A, 200 Recorro la lista de artistas Despues compruebo si
	 * tienen una S o una A Por ultimo el artista debe tener fijado un concierto, ya
	 * que un artista de por si no tiene un precio de entrada
	 * 
	 * @param dni Recibo el dni para ver los artistas
	 */
	public void ArtistasTop(String dni) {
		for (Artista artista : listaArtistas) {
			if (artista != null && artista.getDni().equals(dni)) {
				if (artista.getCache() == 'S') {
					for (Concierto concierto : listaConciertos) {
						if (concierto.getArtistaConcierto().getDni().equals(dni)) {
							concierto.setPrecioEntrada(250);
							System.out.println(concierto);
						} else {
							System.out.println("El artista debe tener un concierto fijado");
						}
					}

				} else if (artista.getCache() == 'A') {
					for (Concierto concierto : listaConciertos) {
						if (concierto.getArtistaConcierto().getDni().equals(dni)) {
							concierto.setPrecioEntrada(200);
							System.out.println(concierto);
						} else {
							System.out.println("El artista debe tener un concierto fijado");
						}
					}
				}
			} else {
				System.out.println("El artista no es TOP");
			}
		}
	}

	/**
	 * Este metodo modificara el honorario de los artistas si realizan mas de 2 o 3
	 * horas Recorremos los conciertos, y si estos duran mas de 2 horas recibiran
	 * 20k mas Si son mas de 3 horas, 25k mas Si duran menos de 2 horas o mas de 3,
	 * no reciben extra
	 * 
	 * @param nombre Recibo el nombre del concierto
	 * @return devuelvo los conciertos con su aumento
	 */
	public Concierto honorarioArtistas(String nombre) {
		int honorarioExtra2 = 20000;
		int honorarioExtra3 = 25000;
		for (Concierto concierto : listaConciertos) {
			if (concierto != null && concierto.getNombre().trim().toLowerCase().equalsIgnoreCase(nombre)) {
				if (concierto.getDuracionHoras() >= 2) {
					honorarioExtra2 += concierto.getHonorarioArtista();
					concierto.setHonorarioArtista(honorarioExtra2);
					return concierto;
				} else if (concierto.getDuracionHoras() >= 3) {
					honorarioExtra3 += concierto.getHonorarioArtista();
					concierto.setHonorarioArtista(honorarioExtra3);
					return concierto;
				} else {
					System.out.println("No corresponde extra");
					return concierto;
				}
			} else {
				System.out.println("Concierto erroneo");
			}

		}
		return null;
	}

	/**
	 * Este metodo unicamente cambia el nombre de un artista por si nos hemos confundido
	 * Recibe el dni del artista del dni que quiere cambiar y el nuevo nombre
	 * @param dni
	 * @return
	 */
	public void cambiarNombreArtista(String dni, String nombreNuevo) {
		for (int i = 0; i < listaArtistas.size(); i++) {
			if (listaArtistas.get(i) != null) {
				if (listaArtistas.get(i).getDni().equals(dni)) {
					listaArtistas.get(i).setNombre(nombreNuevo);
				}
			}
		}
	}

}
